<?php  declare(strict_types=1);
    /**
     * Provides Assertions
     **/
    require ("../vendor/autoload.php");

    if (!function_exists('wp_create_nonce'))
    {

        function wp_create_nonce($action){
            switch ($action) {
                case "test":
                    return  "72f3ff7016";
                case "8b549762e4":
                    return "";
                case "Inpsyde":
                    return "74edaa1075";
            }

        }

    }
    if (!function_exists('wp_nonce_url'))
    {

        function wp_nonce_url($url , $action , $name=_wpnonce){
            return $url."?$name=".wp_create_nonce($action);

        }

    }
    if (!function_exists('esc_url'))
    {

        function esc_url($url){
            return $url;

        }

    }

    class Assert
    {
        public static function AreEqual( $a, $b )
        {
            if ( $a != $b )
            {
                throw new Exception( 'Subjects are not equal.' );
            }
        }
        public static function assertInstanceOf( $a, $b )
        {

            if ( $a != get_class($b) )
            {
                throw new Exception( 'Subjects are not equal.' );
            }
        }

        public static function assertNull( $a )
        {

            if ( $a == null )
            {
                throw new Exception( 'Subjects are null.' );
            }
        }


    }

    /**
     * Provides a loggable entity with information on a test and how it executed
     **/
    class TestResult
    {
        protected $_testableInstance = null;

        protected $_isSuccess = false;
        public function getSuccess()
        {
            return $this->_isSuccess;
        }

        protected $_output = '';
        public function getOutput()
        {
            return $_output;
        }
        public function setOutput( $value )
        {
            $_output = $value;
        }

        protected $_test = null;
        public function getTest()
        {
            return $this->_test;
        }

        public function getName()
        {
            return $this->_test->getName();
        }
        public function getComment()
        {
            return $this->ParseComment( $this->_test->getDocComment() );
        }

        private function ParseComment( $comment )
        {
            $lines = explode( "\n", $comment );
            for( $i = 0; $i < count( $lines ); $i ++ )
            {
                $lines[$i] = trim( $lines[ $i ] );
            }
            return implode( "\n", $lines );
        }

        protected $_exception = null;
        public function getException()
        {
            return $this->_exception;
        }

        static public function CreateFailure( Testable $object, ReflectionMethod $test, Exception $exception )
        {
            $result = new self();
            $result->_isSuccess = false;
            $result->testableInstance = $object;
            $result->_test = $test;
            $result->_exception = $exception;

            return $result;
        }
        static public function CreateSuccess( Testable $object, ReflectionMethod $test )
        {
            $result = new self();
            $result->_isSuccess = true;
            $result->testableInstance = $object;
            $result->_test = $test;

            return $result;
        }
    }

    /**
     * Provides a base class to derive tests from
     **/
    abstract class Testable
    {
        protected $test_log = array();

        /**
         * Logs the result of a test. keeps track of results for later inspection, Overridable to log elsewhere.
         **/
        protected function Log( TestResult $result )
        {
            $this->test_log[] = $result;

            printf( "Test: %s was a %s %s\n"."<br> "
                ,$result->getName()
                ,$result->getSuccess() ? 'success' : 'failure'
                ,$result->getSuccess() ? '' : sprintf( "<br><div style='Color:red'>%s (lines:%d-%d; file:%s)</div>"
                    ,$result->getComment()
                    ,$result->getTest()->getStartLine()
                    ,$result->getTest()->getEndLine()
                    ,$result->getTest()->getFileName()
                )
            );

        }
        final public function RunTests()
        {
            $class = new ReflectionClass( $this );
            foreach( $class->GetMethods() as $method )
            {
                $methodname = $method->getName();
                if ( strlen( $methodname ) > 4 && substr( $methodname, 0, 4 ) == 'Test' )
                {
                    ob_start();
                    try
                    {
                        $this->$methodname();
                        $result = TestResult::CreateSuccess( $this, $method );
                    }
                    catch( Exception $ex )
                    {
                        $result = TestResult::CreateFailure( $this, $method, $ex );
                    }
                    $output = ob_get_clean();
                    $result->setOutput( $output );
                    $this->Log( $result );
                }
            }
        }
    }

    /**
     * a simple Test suite with two tests
     **/

    class MyTest extends Testable
    {
        /**
         * Test action.
         *
         * @var    string $test_action The default test action value.
         */
        private $test_action;
        /**
         * Test nonce.
         *
         * @var    string $test_action The default test nonce value.
         */
        private $test_nonce;
        /**
         * Test validator.
         *
         * @var    object $test_ng The default test generator object.
         */
        private $test_creator;

        public function __construct() {
            $this->test_creator = new  \Inpsyde\Nonce\NonceCreator("Inpsyde");
        }


        /**
         * This test is designed to fail : Check nonce of "test" with Nonce of "Fail"
         **/
        public function Test_create_nonce_failor()
        {
            $this->test_creator->changeAction("test");
            $nonce_generated = $this->test_creator->createNonce();
            // Check the nonce.
            Assert::AreEqual( $nonce_generated, wp_create_nonce("Fail") );
        }
        /**
         * This test is for create_nonce
         **/
        public function Test_create_nonce() {
            // Create  nonce.
            $this->test_creator->changeAction("test");
            $nonce_generated = $this->test_creator->createNonce();
            // Check the nonce.
            Assert::AreEqual( $nonce_generated, wp_create_nonce("test") );
        }
        /**
         * This test is for create_nonce_url
         **/
        public function Test_create_nonce_url(){
            // Generate the nonce and build the url.
            $this->test_creator->changeAction("test");
            $url_generated = $this->test_creator->createNonceUrl( 'http://www.navid.com' );
            // Building the expected url.

            $url_expected = 'http://www.navid.com?_wpnonce='.  $this->test_creator->nonce();
            //echo $url_generated."<br>".$url_expected;die();
            // Checking result.
            Assert::AreEqual( $url_generated, $url_expected);
        }
        public function TestInstance()
        {
            $creator = new  \Inpsyde\Nonce\NonceCreator("Inpsyde");
            Assert::assertInstanceOf( "Inpsyde\Nonce\NonceCreator", $creator );
        }
    }

    // this is how to use it.
    //echo wp_create_nonce("test");
    $test = new MyTest();
    $test->RunTests();