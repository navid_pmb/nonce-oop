<?php
    /**
     * Created by PhpStorm.
     * User: navid
     * Date: 12/17/2019
     * Time: 12:29 PM
     */

    namespace Inpsyde\Nonce;

        function URLValidate($url) : bool {

            if (filter_var($url, FILTER_VALIDATE_URL) === FALSE)
                return false;
            return true;
        }
        function ClearNonce(string $param):string{
            return wp_unslash(sanitize_text_field( $param) );
        }
        function myErrorHandler($errno, $errstr, $errfile, $errline) {
            echo "<br><b>Custom error:</b> [$errno] $errstr<br>";
            echo " Error on line $errline in $errfile<br>";
        }


