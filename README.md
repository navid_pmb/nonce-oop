# nonce-oop
Das Paket implementiert die WordPress Nonces-Funktionalität (wp_nonce _ * ()) als objektorientiert .
## Voraussetzung
- PHP 7.1
- Composer

### Führen Sie die Tests aus
Führen Sie die Datei /test/Custom_test.php im Browser aus

# Mein Idea
Ich implementiere die Nonces-Funktionalität von Wordpress in der OOP-Klasse, ich habe bereits eine Interface und eine Abstract dafür. In NonceAbstarct (implementiert von NonceInterface) habe ich die Grundfunktionen wie ChangeName oder ChangeNonce oder ChangeAction und so weiter implementiert. 
Die Funktionalität von Wordpress Nonce ist in der NonceCreator-Klasse implementiert, die um NonceAbstarct erweitert wurde. 
Wegen der Priorität von $ _Get verwende ich eine andere Klasse (SuperGlobalHandle), um $ _Request zu verarbeiten, und bezüglich der Abhängigkeitsinjektion (DI) verwende ich diese Klasse, um NonceValidate zu implementieren (die Idee von SuperGlobalHandle stammt aus diesem Quellcode https://github.com/Brain-WP/nonces/blob/master/src/RequestGlobalsContext.php)

Dann überprüfte ich meinen Code mit "Inpsyde PHP Codex".

ich habe schon eine set_error_handler Funktion geschrieben, die als Kommentar in der Produktion verwendet werden soll.

### Meine problematische Herausforderung ist ein Testgerät ohne WordPress-Installation
Als Testunit wollte ich zuerst PHPunit als 3D-Paket verwenden, dachte dann aber, dass meine Testunit keine WordPress-Installation als Voraussetzung sein sollte, also schrieb ich eine Custom Testunit und die Funktionen, die eine WordPress-Installation benötigt, habe ich als Pseudocode geschrieben und eingefügt In meiner custom Test-Klasse.
Unter können Sie das Beispiel von "wp_create_nonce" sehen, das ich als Pseudocode für Test geschrieben habe.

```
if (!function_exists('wp_create_nonce'))
    {

        function wp_create_nonce($action){
            switch ($action) {
                case "test":
                    return  "72f3ff7016";
                case "8b549762e4":
                    return "";
                case "Inpsyde":
                    return "74edaa1075";
            }

        }

    }
```

# Verwendung
Füge einfach autoload.php zu deinem Code oder Plugin hinzu
```
require ("inc/inpsyde/vendor/autoload.php");
```
Erstellen Sie eine neue Instanz von NonceCreator und rufen Sie die Methode auf
```
$creator = new  \Inpsyde\Nonce\NonceCreator("Inpsyde","hell1o");

$creator->createNonce();
$creator->name();
$creator->createNonceUrl("http://navid.com",true);
$creator->createNonceField();
$creator->validateRequest();
```
   

# Lizenz
 GPLv2+
 (https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)