<?php declare(strict_types=1);
    /**
     * Created by PhpStorm.
     * User: navid
     */

    namespace Inpsyde\Nonce;

abstract class NonceAbstract implements NonceInterface
{
    /*
         * Create the abstract of Nonce         *
         *
     */
    // name of the nonce
    private $name;
    //action of the nonce
    private $action;
    // value of nonce
    private $nonce;

    /*
         *
         * Construction of NonceAbstract and assign values
         *
     */
    public function __construct(string $action, string $name = '_wpnonce')
    {
        $this->changeAction($action);
        $this->changeName($name);
        $this->changeNonce("");
    }

    /**
     * Get action property.
     *
     * @return string $action
     */
    public function action():string
    {
        return $this->action;
    }
    /**
     * Set action property.
     *
     * @param string $action
     * @return string $action
     */
    public function changeAction(string  $action) :string
    {
        $this->action = $action;
        return $this->action();
    }
    /**
     * Get request name property.
     *
     * @return string $name
     */
    public function name():string
    {
        return $this->name;
    }
    /**
     * Set request name property.
     *
     * @param string $name
     * @return string $name
     */
    public function changeName(string $name):string
    {
        $this->name = $name;
        return $this->name();
    }
    /**
     * Get nonce property.
     *
     * @return string $nonce.
     */
    public function nonce():string
    {
        return $this->nonce;
    }
    /**
     * Set nonce property.
     *
     * @param string $nonce
     * @return string $nonce
     */
    public function changeNonce(string $nonce = ""):string
    {
        $this->nonce = $nonce;
        return $this->nonce();
    }
    public function validateRequest(SuperGlobalHandle $nonce = null): bool{

    }
}
