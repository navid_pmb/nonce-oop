<?php declare(strict_types=1);
    /**
     * Created by PhpStorm.
     * User: navid
     */

    namespace Inpsyde\Nonce;


    class SuperGlobalHandle
    {
        private $context;
        public function __construct()
        {
            //copy from https://github.com/Brain-WP/Nonces/blob/master/src/RequestGlobalsContext.php
            $http_method = empty($_SERVER['REQUEST_METHOD']) ? null : $_SERVER['REQUEST_METHOD'];
            $is_post = is_string($http_method) && strtoupper($http_method) === 'POST';
            $request = $is_post ? array_merge($_GET, $_POST) : $_REQUEST;
            $this->context = $request;
        }
        public function getKey(string $key):string
        {
            return array_key_exists($key,$this->context)?  $this->context[$key] : "";
        }
    }