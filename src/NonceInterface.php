<?php declare(strict_types=1);
    /**
     * User: navid
     *
     */

    namespace Inpsyde\Nonce;

    /////Basic interface for using WordPress None as an OOP
interface NonceInterface
{

    /**
     * Get action property.
     **
     * @return string $action Action value.
     */
    public function action() : string;
    /**
     * Set  action property.
     *
     * @param string $action Action value.
     * @return string $action      Action value set.
     */
    public function changeAction(string $action) : string;
    /**
     * Get name property.
     *
     * @return string $name The nonce name value.
     */
    public function name(): string;
    /**
     * Set name property and return it
     *
     * @param string $name Name for the nonce value.
     * @return string $name      The nonce name value set.
     */
    public function changeName(string $name): string;
    /**
     * Get signature for nonce property.
     *
     * @return string $nonce Nonce value.
     */
    public function nonce(): string;
    /**
     * Set signature for nonce property.
     *
     * @param string $nonce Nonce value to set.
     * @return string $nonce      Nonce value set.
     */
    public function changeNonce(string $nonce): string;
    /**
     * check validity of nonce.
     *
     * @param SuperGlobalHandle $nonce Nonce value to set.
     * @return bool $nonce      Nonce value set.
     */
    public function validateRequest(SuperGlobalHandle $nonce = null): bool;
}
