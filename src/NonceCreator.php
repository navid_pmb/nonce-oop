<?php declare(strict_types=1);
    /**
     * Created by PhpStorm.
     * User: navid
     * Time: 3:13 PM
     */

    namespace Inpsyde\Nonce;

class NonceCreator extends NonceAbstract
{
    /**
     * NonceGenerator constructor.
     *
     * @param $action
     * @param string $name
     */

    public function __construct(string $action, string $name = '_wpnonce')
    {
        set_error_handler("Inpsyde\Nonce\myErrorHandler");
        parent::__construct($action, $name);
    }
    /**
     * Generate nonce string
     *
     * @return string
     */
    public function createNonce():string
    {
        $this->changeNonce(wp_create_nonce($this->action()));
        return $this->nonce();
    }
    /**
     * Generate url with nonce value parameter
     *
     * @param string $url
     * @param bool $checkValidate check the irl is valid or not
     * @return string "null when is not validate, URL when is valid url"
     */
    public function createNonceUrl(string $url, bool $checkValidate = false):string
    {
        $esc = esc_url($url);
        if ($checkValidate && !Helper::URLValidate($esc)) :
            trigger_error("'".$esc ."' is not Valid URL");
            return "";
        endif;
        $name = $this->name();
        $action = $this->action();
        $actionurl = str_replace('&amp;', '&', $esc);
        return wp_nonce_url($actionurl, $action, $name);
    }
    /**
     * Validate the nonce.
     *
     * @return    boolean $is_valid False if the nonce is invalid. Otherwise, returns true.
     */
    private function validate():bool
    {
        $isValid = wp_verify_nonce($this->nonce(), $this->action());
        if (false === $isValid) {
            return false;
        }

        return true;
    }
    /**
     * Validate the nonce from the request.
     * @param SuperGlobalHandle $nonce
     * @return bool  False if the nonce is invalid or not set. Otherwise, returns true.
     */
    public function validateRequest(SuperGlobalHandle $nonce = null):bool
    {
        $nonce = $nonce === null ? new SuperGlobalHandle() : $nonce;
        $this->changeNonce($nonce->getKey($this->name()));
        return $this->validate();
    }
    /**
     * Validate nonce
     *
     * @param $nonce
     * @return bool
     */
    public function validateNonce(string $nonce):bool
    {
        $isValid = false;
        $this->changeNonce($nonce);
        $isValid = $this->validate();
        return $isValid;
    }

    /**
     * Generate the nonce field html tags
     *
     * @param bool $referer
     * @param bool $echo
     * @return string
     */
    public function createNonceField(bool $referer = true, bool $echo = true):string
    {
        $name = esc_attr($this->name());
        $action = $this->action();
        return wp_nonce_field($action, $name, $referer, $echo);
    }
}
